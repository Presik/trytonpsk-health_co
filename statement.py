# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields

from trytond.pool import PoolMeta


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'
    copayment = fields.Boolean('Copayment')

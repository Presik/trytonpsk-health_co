# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import product
from . import invoice
from . import health
from . import health_service
from . import configuration
from . import statement
from . import reports
from . import country


def register():
    Pool.register(
        configuration.Configuration,
        health.PatientEvaluation,
        health_service.HealthService,
        health_service.HealthServiceLine,
        party.Party,
        product.Product,
        product.PriceListLine,
        invoice.Invoice,
        invoice.InvoiceLine,
        country.Subdivision,
        # country.DaneSubdivision,
        reports.PrintRipsStart,
        reports.ProductInvoiceStart,
        reports.GeneralEvaluationPatientStart,
        statement.StatementLine,
        module='health_co', type_='model')
    Pool.register(
        # country.DaneSubdivisionUpdate,
        invoice.CreateServiceInvoice,
        reports.PrintRips,
        reports.GeneralEvaluationPatient,
        reports.ProductInvoice,
        module='health_co', type_='wizard')
    Pool.register(
        invoice.InvoiceHealthReport,
        reports.RipUsuarios,
        reports.RipTransacciones,
        reports.RipConsulta,
        reports.RipOtrosServicios,
        reports.RipMedicamentos,
        reports.RipUrgencias,
        reports.RipRecienNacidos,
        reports.RipHospitalizacion,
        reports.GeneralEvaluationPatientReport,
        reports.ProductInvoiceReport,
        module='health_co', type_='report')

# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction

STATES = {
    'readonly': Eval('state') != 'draft',
}
_ZERO = Decimal('0')


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    amount_copayment = fields.Numeric('Amount Copayment', digits=(16, 2),
        states={'readonly': True})
    # amount_moderator = fields.Numeric('Amount Moderator', digits=(16, 2),
    #     states={'readonly': True})
    net_amount = fields.Function(fields.Numeric(
        'Net Amount', digits=(16, 2)), 'get_net_amount')
    total_payment_shared = fields.Function(fields.Numeric(
        'Total Payment Shared', digits=(16, 2)), 'get_total_payment_shared')

    def get_total_payment_shared(self, name):
        res = _ZERO
        if self.amount_copayment:
            res += self.amount_copayment
        # if self.amount_moderator:
        #     res += self.amount_moderator
        return res

    def get_net_amount(self, name):
        return self.total_amount - self.total_payment_shared


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'
    copayment = fields.Numeric('Copayment', digits=(16, 2))
    service_start = fields.Date('Service Start')
    service_end = fields.Date('Service End')


class InvoiceHealthReport(Report):
    __name__ = 'account.invoice_health'

    @classmethod
    def get_context(cls, records, data):
        report_context = super().get_context(records, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(Transaction().context['company'])
        return report_context


class CreateServiceInvoice(metaclass=PoolMeta):
    __name__ = 'gnuhealth.service.invoice.create'

    def get_invoice_data(self, party, service):
        res = super().get_invoice_data(party, service)
        # res['amount_moderator'] = service.moderator_fee
        res['amount_copayment'] = service.copayment
        return res

    def get_invoice_line(self, line, unit_price, seq):
        InvoiceLine = Pool().get('account.invoice.line')
        res = super().get_invoice_line(line, unit_price, seq)
        if hasattr(InvoiceLine, 'gross_unit_price'):
            res['gross_unit_price'] = unit_price
        return res

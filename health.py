# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.evaluation'
    # ????????
    # patient_info = fields.Function(fields.One2Many('health.patient',
    #     'evaluation', 'Patient Data'), 'get_patient_info')
    # muscle_mass = fields.Char('Muscle Mass')
    # metabolic_rate = fields.Char('Metabolic Rate')
    # right_thigh = fields.Char('Right Thigh')
    # left_thigh = fields.Char('Left Thigh')
    # right_arm = fields.Char('Right Arm')
    # left_arm = fields.Char('Left Arm')
    # pre_glucose = fields.Char('Pre-Glucose')
    # pos_glucose = fields.Char('Pos-Glucose')
    # vitaminD3 = fields.Char('vitamin D3')
    # homocysteine = fields.Char('Homocysteine')
    # uric_acid = fields.Char('Uric Acid')
    # tsh = fields.Char('TSH')
    # reactive_protein_c = fields.Char('Reactive Protein C')
    # ggt = fields.Char('GGT')
    # mg = fields.Char('Mg++')
    # pre_insulin = fields.Char('Pre Insulin')
    # pos_insulin = fields.Char('Pos Insulin')
    # level_fat_visc = fields.Char('Level Fat Visc')

    def get_patient_info(self, name):
        return [self.patient.id, ]


class InsurancePlan(metaclass=PoolMeta):
    __name__ = 'gnuhealth.insurance.plan'
    code = fields.Char('Code', required=True)
    contract = fields.Char('Contract')

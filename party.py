# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval

MAP_IDENTIFIER = {
    'CC': '13',
    'CE': '22',
    'PA': '41',
    'RC': '11',
    'TI': '12',
    'AS': '43',
    'MS': '43',
    'NU': '',
}

STATES_PATIENT = {
    'invisible': ~Eval('is_patient')
}


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    type_user = fields.Selection([
        ('1', '1 - Contributivo'),
        ('2', '2 - Subsidiado'),
        ('3', '3 - Vinculado'),
        ('4', '4 - Particular'),
        ('5', '5 - Otro'),
        ('7', '7 - Desplazados con afiliacion al regimen contributivo'),
        ('8', '8 - Desplazados con afiliacion al regimen subsidiado'),
        ('9', '9 - Desplazado no asegurado (vinculado)'),
        ('', ''),
        ], 'Type User', states=STATES_PATIENT)
    type_user_string = type_user.translated('type_user')
    residential_area = fields.Selection([
        ('U', 'Urbana'),
        ('R', 'Rural'),
        ('', ''),
        ], 'Residential Area', states=STATES_PATIENT)

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        new_state_name = {'invisible': ~Eval('is_person')}
        cls.first_name.states = new_state_name
        cls.second_name.states = new_state_name
        cls.first_family_name.states = new_state_name
        cls.second_family_name.states = new_state_name

    @staticmethod
    def default_citizenship():
        Country = Pool().get('country.country')
        country_co, = Country.search_read([('code', '=', 'CO')])
        return country_co['id']

    @staticmethod
    def default_residence():
        Country = Pool().get('country.country')
        country_co, = Country.search_read([('code', '=', 'CO')])
        return country_co['id']

    @staticmethod
    def default_residential_area():
        return 'U'

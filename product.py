# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    uvr = fields.Float('Unidad Valor Relativo', select=True)
    type_uvr = fields.Selection([
            ('soat', 'SOAT'),
            ('iss', 'ISS'),
            ('', ''),
            ], 'Type UVR')


class PriceListLine(metaclass=PoolMeta):
    __name__ = 'product.price_list.line'
    copayment_formula = fields.Char('Copayment Formula')
    # max_copayment_amount = fields.Numeric('Max Copayment Amount',
    #     digits=(16, 2))

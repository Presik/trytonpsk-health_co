# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pyson import Eval


class Configuration(ModelSQL, ModelView):
    "Configuration"
    __name__ = 'gnuhealth.configuration'
    company = fields.Many2One('company.company', 'Company', required=True)
    account_copayment = fields.Many2One('account.account', 'Account Copayment',
        domain=[
            ('company', '=', Eval('context', {}).get('company', -1)),
            ('kind', '!=', 'view'),
        ])
    account_moderator_fee = fields.Many2One('account.account',
        'Account Moderator Fee', domain=[
            ('company', '=', Eval('context', {}).get('company', -1)),
            ('kind', '!=', 'view'),
        ])
    product_copayment = fields.Many2One('product.product', 'Producto Copago',
        domain=[('type', '=', 'service')])
